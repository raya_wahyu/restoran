const dataMakanan =[
{
	foto:"https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_300,h_300/https://sambelmertua.com/wp-content/uploads/2013/09/Sambel-Teri-Cap-Mertua-300x300.jpg", 
	nama: 'sambal Teri',
	harga: "Rp15.000"
}, 
{
	foto:"https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_300,h_300/https://sambelmertua.com/wp-content/uploads/2013/09/Sambel-Ebi-Cap-Mertua2-300x300.jpg", 
	nama: 'Sambal Ebi', 
	harga: "Rp15.000"
}, 
{
	foto:"https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_300,h_300/https://sambelmertua.com/wp-content/uploads/2013/09/sambal-bawang-kering-cap-mertua-baru-300x300.jpg", 
	nama: 'Sambal Bawang', 
	harga: "Rp15.000"
}, 
{
	foto:"https://s0.bukalapak.com/img/06262733831/large/data.png", 
	nama: 'Sambal Gledeg', 
	harga: "Rp15.000"
},  
{
	foto:"https://p7.hiclipart.com/preview/22/939/689/spanish-rice-nasi-goreng-pilaf-mexican-cuisine-arroz-con-pollo-spanish-rice-thumbnail.jpg ", 
	nama: 'nasi Goreng', 
	harga: "Rp18.000"
}, 
{
	foto:"https://warkophade.files.wordpress.com/2016/11/es-teh-m-300x300.png", 
	nama: 'Es Teh', 
	harga: "Rp5.000"
},  
{
	foto:"https://img1.pngdownload.id/20171221/tge/orange-juice-png-image-5a3c21f5019f64.4479054315138902930067.jpg", 
	nama: 'Jus Jeruk',
	harga: "Rp5.000"
},  
{
	foto:"https://i2.wp.com/www.caminodesantiago.me/wp-content/uploads/water-bottle.jpg?fit=432%2C432&ssl=1", 
	nama: 'Air Mineral', 
	harga: "Rp4.500"
},  
{
	foto:"https://img1.pngdownload.id/20180624/riy/kisspng-coffee-production-in-indonesia-secangkir-kopi-kopi-5b2f84dd81a524.647594821529840861531.jpg", 
	nama: 'Kopi', 
	harga: "Rp5.000"
}, 
 {
 	foto:"https://i7.pngguru.com/preview/467/313/140/beer-soft-drink-coca-cola-drink-thumbnail.jpg", 
 	nama: 'Soft Drink', 
 	harga: "Rp9.000"
 },
 ];


 const clMap = (data , index)=>{
		const card = document.querySelector('.card-deck');
	card.innerHTML += `<div class="box-makanan ">
		  		<img src="${data.foto}" style="height:200px; width:300px;">
			    <h5 class="card-title">${data.nama}</h5>
			    <p class="card-text">${data.harga}</p>
			    <a href="#" class="btn">Beli</a>
			 
	</div> `;


}

dataMakanan.map(clMap);

const buttonElmnt = document.querySelector('.btn');
buttonElmnt.addEventListener('click', ()=>{
		const hasilSearch = dataMakanan.filter((data, index)=>{
					const inputElmnt  = document.querySelector('.form-control');
					const kunciSearch =inputElmnt.value.toLowerCase();
					const namaMakanan = data.nama.toLowerCase();
					return namaMakanan.includes(kunciSearch);
					
						})

		document.querySelector('.card-deck').innerHTML ='';
		hasilSearch.map(clMap);
});

